const multer = require('multer');
const path = require('path');

/**
 * the storage module definition
 */
var stockage = multer.diskStorage({
    destination: function (req, file, cb) {
        return cb(null, path.join(__dirname + '../../../clients/'));
    },
    filename: function (req, file, cb) {
        return cb(null, file.fieldname + '-' + Date.now() + '.' + file.originalname.split('.')[1]);
    }
});
var upload = multer({
    storage: stockage
});

module.exports = app => {
    const client = require('../controllers/client.controller');

    var router = require('express').Router();

    // Create a new client
    router.post("/add",  upload.single('file'), function (req, res) {

        res.setHeader('Access-Control-Allow-Origin', '*');
        // console.log(req.file);
        if (req.file) {
            client.create(req, res, req.file.filename);
        } else {
            client.create(req, res, null);
        }
    });

    // Retrieve all client
    router.get("/", client.findAll);

    // Retrieve a single client with id
    router.get("/:id", client.findOne);

    // Update a client with id
    router.put("/:id",  upload.single('file'), function (req, res) {

        res.setHeader('Access-Control-Allow-Origin', '*');
        // console.log(req.file);
        if (req.file) {
            client.update(req, res, req.file.filename);
        } else {
            client.update(req, res, null);
        }
    });

    // Delete a client with id
    router.delete("/del/:id", client.delete);

    // Retrieve a single client with id
    router.post("/code", client.findByCode);

    app.use('/api/client', router);
};