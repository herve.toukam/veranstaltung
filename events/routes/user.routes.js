module.exports = app => {
    const users = require('../controllers/user.controller');

    var router = require('express').Router();

    // Create a new user
    router.post("/add", users.create);

    // Retrieve all users
    router.get("/", users.findAll);

    // Retrieve a single user with id
    router.get("/:id", users.findOne);

    // Update a user with id
    router.put("/:id", users.update);

    // Delete a user with id
    router.delete("/del/:id", users.delete);
    
    // Authenticate a new user
    router.post("/login", users.login);

    app.use('/api/users', router);
};