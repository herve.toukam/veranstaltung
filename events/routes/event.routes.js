const multer = require('multer');
const path = require('path');

/**
 * the storage module definition
 */
var stockage = multer.diskStorage({
    destination: function (req, file, cb) {
        return cb(null, path.join(__dirname + '../../../uploads/'));
    },
    filename: function (req, file, cb) {
        return cb(null, file.fieldname + '-' + Date.now() + '.' + file.originalname.split('.')[1]);
    }
});
var upload = multer({
    storage: stockage
});

module.exports = app => {
    const event = require('../controllers/event.controller');

    var router = require('express').Router();

    // Create a new event
    router.post("/add", upload.single('file'), function (req, res) {

        res.setHeader('Access-Control-Allow-Origin', '*');
        console.log(req.body);
        if (req.file) {
            event.create(req, res, req.file.filename);
        } else {
            event.create(req, res, null);
        }
    });

    // Retrieve all event
    router.get("/", event.findAll);

    // Retrieve a single event with id
    router.get("/:id", event.findOne);

    // Update a event with id
    router.put("/:id", upload.single('file'), function (req, res) {

        res.setHeader('Access-Control-Allow-Origin', '*');
        console.log(req.file);
        if (req.file) {
            event.update(req, res, req.file.filename);
        } else {
            event.update(req, res, null);
        }
    });

    // Delete a event with id
    router.delete("/del/:id", event.delete);

    app.use('/api/event', router);
};