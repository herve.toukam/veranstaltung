(function ($) {

    window.localStorage.getItem('iduser') == null ? window.location = './index.html' : null;

    var Card = {
        container: null,
        initilize: function (id, titre, description, debut, photo, salle) {
            this.container = $('<div class="col-md-3"></div>').attr('id', id)
                .append($('<div></div>').attr('class', 'item-box-blog')
                    .append($('<div></div>').attr('class', 'item-box-blog-image')
                        .append($('<div></div>').attr('class', 'item-box-blog-date bg-blue-ui white')
                            .append($('<span></span>').attr('class', 'mon').html(moment(new Date(debut)).format('MMM, DD'))
                            )
                        )
                        .append($('<figure></figure>')
                            .append($('<img alt="">').attr('src', photo))
                        )
                    )
                    .append($('<div></div>').attr('class', 'item-box-blog-body')
                        .append($('<div></div>').attr('class', 'item-box-blog-heading')
                            .append($('<a href="#" tabindex="0"></a>')
                                .append($('<h5></h5>').html(titre))
                            )
                            .append($('<button></button>').attr('class', 'btn btn-danger').html('Stop')
                            .on('click', function () {
                                disableEvent(id);
                            }))
                        )
                        .append($('<div style="padding: px 15px;"></div>').attr('class', 'item-box-blog-data')
                            .append($('<p></p>')
                                // .append($('<i></i>').attr('class', 'fa fa-user-o'))
                                // .html('Start:' + moment(new Date(debut)).format('MMM, DD H:m:s'))
                                .append($('<i></i>').attr('class', 'fa fa-comments-o'))
                                .html('Start: ' + moment(new Date(debut)).format('MMM, DD H:m:s'))
                            )
                        )
                        .append($('<div></div>').attr('class', 'mt row')
                            .append($('<a href="#" tabindex="0" class="btn bg-blue-ui white read"></a>').html('View List')
                                .on('click', function () {
                                    loadClientByEventId(id);
                                    $('#cache').val(id);
                                }))
                            .append($('<a href="#" target="_blank" class="btn btn-primary " data-toggle="modal" data-target="#edit" style="border-radius: 50px; font-size: 13px; margin: 5px; bottom: 3px"></a>')
                                .on('click', function () {
                                    $('#editedTitre').val(titre);
                                    $('#editedStart').val(debut);
                                    $('#idevent').val(id);
                                }).append($('<i></i>').attr('class', 'fas fa-edit'))
                            )
                            .append($('<a href="#" target="_blank" class="btn btn-danger" data-toggle="modal" data-target="#dele" data-id="' + id + '" style="border-radius: 50px; font-size: 13px; margin: 5px;"></a>')
                                .on('click', function () {
                                    $('#id_event').val(id);
                                }).append($('<i></i>').attr('class', 'fas fa-trash'))
                            )
                        )
                    )
                );
        },
        bind: function () {
            $('.events').append(this.container);
        }
    };

    $.ajax({
        url: chemins.events.getEventsList,
        type: 'GET',
        headers: {
            Authorization: window.localStorage.getItem('token')
        },
        // datatype: JSON,
        success: function (data, textStatus, jqXHR) {
            if (data.data != 0) {
                for (let i = 0; i < data.data.length; i++) {
                    Card.initilize(data.data[i].idevent, data.data[i].titre, data.data[i].description, data.data[i].debut, data.data[i].logo, data.data[i].idroom);
                    Card.bind();
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
        },
        async: true
    });

    function loadClientByEventId(id) {
        /**
         * load data of a participant's in the table
         */
        $('#listClient').dataTable({
            "destroy": true,
            "processing": true,
            "ajax": {
                "url": chemins.clients.getClientByEvent + "" + id,
                "headers": {
                    "Authorization": window.localStorage.getItem('token')
                },
                "type": "GET",
                "dataSrc": "data"
            },
            "columns": [{
                "data": "idclient"
            }, {
                "data": "fullname"
            }, {
                "data": "email"
            }, {
                "data": "signature"
            }],
            "columnDefs": [{
                "orderable": false,
                "className": 'select-checkbox',
                "targets": 0
            }],
            "select": {
                "style": 'os',
                "selector": 'td:first-child'
            },
            "order": [[2, 'asc']]
        });
    }

    $('#addEvent').on('click', function () {
        var form = new FormData();
        form.append("roomIdroom", $('#rooms').val());
        form.append("titre", $('#titre').val());
        form.append("debut", $('#start').val());
        form.append("file", $('#logo').get(0).files[0]);
        $.ajax({
            url: chemins.events.createEvent,
            processData: false,
            contentType: false,
            data: form,
            type: 'POST',
            datatype: 'JSON',
            headers: {
                Authorization: window.localStorage.getItem('token')
            },
            success: function (data, textStatus, jqXHR) {
                if (data.message == 'success') {
                    window.location.reload();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            async: true
        });
    });

    $('#editEvent').on('click', function () {
        var form = new FormData();
        form.append('roomIdroom', $('#editedRooms').val());
        form.append('titre', $('#editedTitre').val());
        form.append('debut', $('#editedStart').val());
        form.append('file', $('#editedLogo').get(0).files[0]);
        $.ajax({
            url: chemins.events.editEvent + "" + $('#idevent').val(),
            processData: false,
            contentType: false,
            data: form,
            type: 'PUT',
            datatype: 'JSON',
            headers: {
                Authorization: window.localStorage.getItem('token')
            },
            success: function (data, textStatus, jqXHR) {
                if (data.message == 'success') {
                    window.location.reload();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            async: true
        });
    });

    $('#dele').on('show.bs.modal', function (e) {
        var data = $(e.relatedTarget).data('id');
        $('#id_event').val(data);
        console.log(data);
    });

    $('#delEvent').on('click', function () {
        var event = {
            statut: 0
        };
        $.ajax({
            url: chemins.events.editEvent + "" + $('#id_event').val(),
            data: event,
            type: 'PUT',
            headers: {
                Authorization: window.localStorage.getItem('token')
            },
            success: function (data, textStatus, jqXHR) {
                if (data.message == 'success') {
                    window.location.reload();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            async: true
        });
    });

    $('#addP').on('show.bs.modal', function (e) {
        var data = $('#cache').val();
        if (data == null || data == undefined) {
            alert('Please select Event first!');
        }
    });

    $('#createClient').on('click', function () {
        var data = $('#cache').val();
        if (data == null || data == undefined) {
            alert('Please select Event first!');
        } else {
            var client = {
                evenementIdevent: $('#cache').val(),
                fullname: $('#clientFullname').val(),
                phone: $('#clientPhone').val(),
                email: $('#clientEmail').val(),
                sexe: $('#clientSexe').val(),
                association: $('#clientAssoc').val(),
                categorieage: $('#clientAge').val(),
                adresse: $('#clientAddr').val(),
                code: getCode()
            };
            // console.log(client);
            if (client.association == '' || client.categorieage == '' || client.email == '' || client.fullname == '' || client.phone == '' || client.sexe == '' || client.adresse == '') {
                alert('Please complete your personal informations!!');
            } else {
                $.ajax({
                    url: chemins.clients.addParticipant,
                    type: 'POST',
                    data: client,
                    headers: {
                        Authorization: window.localStorage.getItem('token')
                    },
                    success: function (data) {
                        if (data.message == 'success') {
                            // notifyClient(client.email, client.code);
                            window.location.reload();
                        } else {
                            alert('An error occur while creating account!');
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(jqXHR) },
                    async: true
                });
            }
        }
    });

    /**
     * generate Code
     */
    function getCode() {
        var code = '';
        for (let i = 0; i <= 5; i++) {
            code += Math.floor(Math.random() * 10);
        }
        return code;
    }

    /**
     * get rooms from server
     */
    function getSalles() {
        $.ajax({
            url: chemins.rooms.getRooms,
            type: 'GET',
            headers: {
                Authorization: window.localStorage.getItem('token')
            },
            success: function (data) {
                populate_salles(data.data);
            },
            error: function (jqXHR, textStatus, errorThrown) { console.log(jqXHR) },
            async: true
        });
    }

    /**
     * function todisplay rooms
     * @param {*} data 
     */
    function populate_salles(data) {
        $('#rooms').append($('<option value="">Chose room</option>'));
        $('#editedRooms').append($('<option value="">Chose room</option>'));
        data.forEach(function (el) {
            $('#editedRooms').append($('<option></option>').attr('value', el.idroom).html(el.label));
            $('#rooms').append($('<option></option>').attr('value', el.idroom).html(el.label));
        });
    }

    getSalles();

    /**
     * method to disable an event
     */
    function disableEvent(id) {
        var event = {
            statut: 0
        };
        $.ajax({
            url: chemins.events.editEvent + "" + id,
            data: event,
            type: 'PUT',
            headers: {
                Authorization: window.localStorage.getItem('token')
            },
            success: function (data, textStatus, jqXHR) {
                if (data.message == 'success') {
                    // $.toast({
                    //     title: 'Notification',
                    //     subtitle: 'Arret de l\'evenement '+ id,
                    //     content: 'Evenement terminé!',
                    //     type: 'Information',
                    //     pause_on_hover: false,
                    //     delay: 5000
                    // });
                    window.location.reload();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            async: true
        });

    }

})(jQuery);