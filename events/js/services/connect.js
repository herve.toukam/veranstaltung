(function ($) {

    $('#connection').on('click', function () {
        $('#error').html();
        var user = {
            email: $('#email').val(),
            password: $('#password').val()
        };
        // alert(Object.values(user));
        if (user.email == '' || user.password == '') {
            alert('Please enter your personal informations!!');
        } else {
            $.ajax({
                url: chemins.users.login,
                type: 'POST',
                data: user,
                success: function (res) {
                    //some code...
                    if (res.data != null) {
                        window.location = './index_admin.html';
                        window.localStorage.setItem('iduser', res.data.iduser);
                        window.localStorage.setItem('token', 'Bearer ' + res.token);
                    } else {
                        $('#error').html(res.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#error').html(/*jqXHR.statusText + */'Failed to connect to the Server!');
                    console.log(jqXHR);
                },
                async: true
            });
        }
    });

})(jQuery);