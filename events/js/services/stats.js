(function ($) {

    window.localStorage.getItem('iduser') == null ? window.location = './index.html' : null;

    /**
     * get events from server
     */
    function getEvents() {
        $.ajax({
            url: chemins.events.statEvents,
            type: 'GET',
            headers: {
                Authorization: window.localStorage.getItem('token')
            },
            success: function (data) {
                populate_events(data.data);
            },
            error: function (jqXHR, textStatus, errorThrown) { console.log(jqXHR) },
            async: true
        });
    }

    /**
     * function todisplay events
     * @param {*} data 
     */
    function populate_events(data) {
        $('#events').append($('<option value="">Chose One Event</option>'));
        data.forEach(function (el) {
            $('#events').append($('<option></option>').attr('value', el.idevent + ',' + el.titre).html(el.titre));
        });
    }

    getEvents();

    function loadBande(titre, data, total) {
        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Events title: ' + titre + ', Total Participants: ' + total
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        pointFormat: '{point.y}'
                    }
                }
            },
            legend: {
                enabled: false
            },
            xAxis: {
                type: 'category',
                allowDecimals: false,
                title: {
                    text: ""
                }
            },
            yAxis: {
                title: {
                    text: "Participants"
                }
            },
            series: [{
                name: '',
                data: data
            }]
        });
    }

    function loadDisque(titre, res, total) {
        Highcharts.chart('container2', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Events title: ' + titre + ', Total Participants: ' + total
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    }
                }
            },
            series: [{
                name: 'Participants',
                colorByPoint: true,
                data: res
            }]
        });
    }

    $('#events').on('change', function () {
        // alert($('#events').val());
        draw($('#events').val().split(',')[0], $('#events').val().split(',')[1]);
    });

    function draw(idevent, titre) {
        var prop = [];
        var percent = 0;
        var somme = 0;
        var first = {
            val: 0,
            index: 0
        };
        $.ajax({
            url: chemins.clients.getClientByEvent + '' + idevent,
            type: 'GET',
            headers: {
                Authorization: window.localStorage.getItem('token')
            },
            success: function (data) {
                for (var h = 0; h < data.stat.length; h++) {
                    percent += parseInt(data.stat[h].y);
                    somme += parseInt(data.stat[h].y);
                    if (first.val <= parseInt(data.stat[h].y)) {
                        first.val = parseInt(data.stat[h].y);
                        first.index = h;
                    }
                }
                for (var a = 0; a < data.stat.length; a++) {
                    if (a == first.index) {
                        prop[a] = {
                            name: data.stat[a].name,
                            y: getPercent(parseInt(data.stat[a].y), percent),
                            sliced: true,
                            selected: true
                        };
                    } else {
                        prop[a] = {
                            name: data.stat[a].name,
                            y: getPercent(parseInt(data.stat[a].y), percent)
                        };
                    }
                }
                loadDisque(titre, prop, somme);
                loadBande(titre, data.stat, somme);
            },
            error: function (jqXHR, textStatus, errorThrown) { console.log(jqXHR) },
            async: true
        });
    }

    function getPercent(val, total) {
        var div = (val / total);
        var mul = (div * 100);
        return mul;
    }


})(jQuery);