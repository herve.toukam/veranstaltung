(function ($) {

    window.localStorage.getItem('iduser') == null ? window.location = './index.html' : null;

    $('#listRoom').dataTable({
        "processing": true,
        "ajax": {
            "url": chemins.rooms.getRooms,
            "headers": {
                "Authorization": window.localStorage.getItem('token')
            },
            "type": "GET",
            "dataSrc": "data"
        },
        "columns": [{
            "data": "idroom"
        }, {
            "data": "label"
        }, {
            "data": "contenance"
        }, {
            "data": "options"
        }],
        "columnDefs": [{
            "orderable": false,
            "className": 'select-checkbox',
            "targets": 0
        }],
        "select": {
            "style": 'os',
            "selector": 'td:first-child'
        },
        "order": [[1, 'asc']]
    });

    $('#edit-room').on('show.bs.modal', function (e) {
        var data = $(e.relatedTarget).data('id');
        $('#idroom').val(data.split(',')[0]);
        $('#editedLabel').val(data.split(',')[1]);
        $('#editedQte').val(data.split(',')[2]);
    });

    $('#addRoom').on('click', function () {
        var Room = {
            iduser: window.localStorage.getItem('iduser'),
            label: $('#label').val(),
            contenance: $('#qte').val()
        };
        // console.log(Room);
        $.ajax({
            url: chemins.rooms.createRoom,
            data: Room,
            type: 'POST',
            headers: {
                Authorization: window.localStorage.getItem('token')
            },
            datatype: JSON,
            success: function (data, textStatus, jqXHR) {
                if (data.message == 'success') {
                    window.location.reload();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            async: true
        });
    });

    $('#editRoom').on('click', function () {
        var Room = {
            label: $('#editedLabel').val(),
            contenance: $('#editedQte').val()
        };
        $.ajax({
            url: chemins.rooms.editRoom + "" + $('#idroom').val(),
            data: Room,
            type: 'PUT',
            headers: {
                Authorization: window.localStorage.getItem('token')
            },
            datatype: JSON,
            success: function (data, textStatus, jqXHR) {
                if (data.message == 'success') {
                    window.location.reload();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            async: true
        });
    });

    $('#delete').on('click', function () {
        var Room = {
            statut: 0
        };
        $.ajax({
            url: chemins.rooms.editRoom + "" + $('#id_del').val(),
            data: Room,
            type: 'PUT',
            headers: {
                Authorization: window.localStorage.getItem('token')
            },
            datatype: JSON,
            success: function (data, textStatus, jqXHR) {
                if (data.message == 'success') {
                    window.location.reload();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            async: true
        });
    });

})(jQuery);