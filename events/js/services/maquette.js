(function ($) {

    window.localStorage.getItem('iduser') == null ? window.location = './index.html' : null;

    function getParameterByName(valeur, url) {
        if (!url)
            url = window.location.href;
        valeur = valeur.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + valeur + "(=([^&#]*)|&|#|$)"), results = regex.exec(url);
        if (!results)
            return null;
        if (!results[2])
            return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    var Ligne = {
        ligne: null,
        initilize: function (id, name, adresse, signature) {
            this.ligne = $('<tr></tr>')
                .append($('<td></td>').attr('class', 'prems').html(id))
                .append($('<td></td>').html(name))
                .append($('<td></td>').html(adresse))
                .append($('<td></td>')
                    .append($('<img alt="">').attr('src', signature)))
        },
        bind: function () {
            $('.clients').append(this.ligne);
        }
    }

    $.ajax({
        url: chemins.events.getEventById + "" + getParameterByName('idevent'),
        type: 'GET',
        headers: {
            Authorization: window.localStorage.getItem('token')
        },
        success: function (data, textStatus, jqXHR) {
            if (data.data != 0) {
                $('#logo').attr('src', data.data.logo);
                $('#titre').html(data.data.titre);
                $('#date').html(moment(new Date(data.data.debut)).format('dddd, DD.MM.YYYY') + ', 44137 Mendong');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
        },
        async: true
    });

    $.ajax({
        url: chemins.clients.getClientByEvent + "" + getParameterByName('idevent'),
        type: 'GET',
        headers: {
            Authorization: window.localStorage.getItem('token')
        },
        success: function (data, textStatus, jqXHR) {
            if (data.data != 0) {
                for (let i = 0; i < data.data.length; i++) {
                    Ligne.initilize(data.data[i].id, data.data[i].fullname, data.data[i].adresse, data.data[i].pp);
                    Ligne.bind();
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
        },
        async: true
    });

})(jQuery);