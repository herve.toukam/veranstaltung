(function ($) {
    
    window.localStorage.getItem('iduser') == null ? window.location = './index.html' : null;
    
    $('#listEvents').dataTable({
        "processing": true,
        "ajax": {
            "url": chemins.events.archives,
            "headers": {
                "Authorization": window.localStorage.getItem('token')
            },
            "type": "GET",
            "dataSrc": "data"
        },
        "columns": [{
            "data": "id"
        }, {
            "data": "titre"
        }, {
            "data": "idroom.label"
        }, {
            "data": "debut"
        }, {
            "data": "consult"
        }],
        "columnDefs": [{
            "orderable": false,
            "className": 'select-checkbox',
            "targets": 0
        }],
        "select": {
            "style": 'os',
            "selector": 'td:first-child'
        },
        "order": [[1, 'asc']]
    });

})(jQuery);