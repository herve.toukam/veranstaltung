const Host = "http://localhost:1234/";
const chemins = {
    users: {
        login: Host + 'api/users/login',
    },
    clients: {
        addParticipant: Host + "api/client/add",
        findClientByCode: Host + "api/client/get",
        sign: Host + "api/client/",
        getClientByEvent: Host + "api/client?idevent="
    },
    events: {
        getEventsList: Host + "api/event?params=1",
        getEventById: Host + "api/event/",
        autoEdit: Host + "api/event/",
        archives: Host + "api/event?params=0",
        createEvent: Host + "api/event/add",
        editEvent: Host + "api/event/",
        statEvents: Host + "api/event",
        disable: Host + 'api/event/disable/'
    },
    rooms: {
        getRooms: Host + "api/room?params=1",
        createRoom: Host + "api/room/add",
        editRoom: Host + "api/room/",
    }
}