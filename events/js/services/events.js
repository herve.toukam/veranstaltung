(function ($) {

    var Card = {
        container: null,
        initialize: function (id, titre, description, debut, fin, photo) {
            this.container = $('<div class="col-lg-4 col-md-4 col-sm-6 col-12 mb-2 mt-4"></div>').attr('id', id)
                .append($('<div></div>').attr('class', 'inforide')
                    .append($('<div></div>').attr('class', 'row')
                        .append($('<div></div>').attr('class', 'col-lg-3 col-md-4 col-sm-4 col-4 rideone')
                            .append($('<img />').attr('src', photo)))
                        .append($('<div></div>').attr('class', 'col-lg-9 col-md-8 col-sm-8 col-8 fontsty')
                            .append($('<h3></h3>').html(titre))
                            .append($('<h4></h4>')
                                .html('End at: ' + moment(new Date(debut)).format('MMM DD, YYYY HH:mm')))
                            .append($('<button data-toggle="modal" onclick="checkDate(debut);" data-target="#addP"></button>').attr('class', 'btn btn-primary ajoutP')
                                .on('click', function () {
                                    $('#idevent').val(id);
                                    moment(new Date()).format('YYYY-MM-DD H:m:s') >= moment(new Date(debut)).format('YYYY-MM-DD H:m:s') ? $('#auth').show() : $('#auth').hide();
                                    $('#dateEvent').val(debut);
                                })
                                .append($('<i></i').attr('class', 'fa fa-plus-circle'))
                            )
                        )
                    )
                );
        },
        bind: function () {
            $('.events').append(this.container);
        }
    };

    function loadEvents() {
        $.ajax({
            url: chemins.events.getEventsList,
            type: 'GET',
            success: function (reponse) {
                //console.log(reponse.eventsList);
                for (var i = 0; i < reponse.length; i++) {
                    Card.initialize(reponse.data[i].idevent, reponse.data[i].titre, reponse.data[i].description, reponse.data[i].debut, reponse.data[i].fin, reponse.data[i].logo);
                    Card.bind();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) { console.log(jqXHR) },
            async: true
        });

        /**
         * rechargement automatique
         */
        setTimeout(() => {
            $('.events').empty();
            loadEvents();
        }, 300000);
    }

    loadEvents();

    $('#create').on('click', function (e) {
        var client = {
            evenementIdevent: $('#idevent').val(),
            fullname: $('#fullname').val(),
            phone: $('#phone').val(),
            email: $('#email').val(),
            sexe: $('#sexe').val(),
            association: $('#assoc').val(),
            categorieage: $('#age').val(),
            adresse: $('#addr').val(),
            code: getCode()
        }
        if (client.association == '' || client.categorieage == '' || client.email == '' || client.fullname == '' || client.phone == '' || client.sexe == '' || client.adresse == '') {
            alert('Please complete your personal informations!!');
        } else {
            $.ajax({
                url: chemins.clients.addParticipant,
                type: 'POST',
                data: client,
                success: function (data) {
                    if (data.message == 'success') {
                        moment(new Date()).format('YYYY-MM-DD H:m:s') >= moment(new Date($('#dateEvent').val())).format('YYYY-MM-DD H:m:s') ? window.location = 'signature/index.html?idclient=' + data.data.idclient : window.location.reload();
                    } else {
                        alert('An error occur while creating account!');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) { console.log(jqXHR) },
                async: true
            });
        }
    });

    $('#sendCode').on('click', function () {
        //116674
        var c = {
            code: $('#code').val()
        };
        console.log(c);
        $.ajax({
            url: chemins.clients.findClientByCode,
            type: 'POST',
            data: { code: $('#code').val() },
            success: function (data) {
                console.log(data);
                if (data.data != null) {
                    // notifyClient(client.email, client.code);
                    window.location = 'signature/index.html?idclient=' + data.data.idclient;
                } else {
                    alert('Please you have entered invalid code, try again!');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) { console.log(jqXHR) },
            async: true
        });
    });

    /**
     * generate Code
     */
    function getCode() {
        var code = '';
        for (let i = 0; i <= 5; i++) {
            code += Math.floor(Math.random() * 10);
        }
        return code;
    }

    function notifyClient(email, code) {
        Email.send({
            // Host: 'smtp.elasticemail.com',
            Username: 'yvestchoumteu55',
            Password: 'D7EF1BDE1D28192C5FE82AE46991C9FA88BE',
            To: email,
            From: 'yvestchoumteu55@gmail.com',
            Subject: 'Validation code',
            Body: 'This is your validation code of the subscribed event: ' + code + ' keep it very well.'
        }).then(
            message => alert(message)
        );
    }

})(jQuery);