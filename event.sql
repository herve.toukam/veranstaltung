-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  jeu. 09 avr. 2020 à 01:44
-- Version du serveur :  10.4.6-MariaDB
-- Version de PHP :  7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `event`
--

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `idclient` int(11) NOT NULL,
  `evenementIdevent` int(11) NOT NULL,
  `fullname` varchar(32) DEFAULT NULL,
  `phone` varchar(32) NOT NULL,
  `email` varchar(50) NOT NULL,
  `sexe` char(1) NOT NULL,
  `association` varchar(50) NOT NULL,
  `categorieage` varchar(50) NOT NULL,
  `adresse` varchar(75) DEFAULT NULL,
  `etat` int(11) NOT NULL,
  `signature` varchar(255) DEFAULT NULL,
  `code` varchar(25) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`idclient`, `evenementIdevent`, `fullname`, `phone`, `email`, `sexe`, `association`, `categorieage`, `adresse`, `etat`, `signature`, `code`, `createdAt`, `updatedAt`) VALUES
(2, 1, 'Yves Loic', '698448214', 'test@gmail.com', 'M', 'TLN&Co', 'adolescent', 'Nomayos', 0, '', '2210186', '2020-04-06 11:13:15', '2020-04-06 11:13:15'),
(3, 1, 'Inew Alex', '698586208', 'ines@gmail.com', 'F', 'Inzzzz', 'adolescent', 'Nomayos', 0, '', '2188610', '2020-04-06 11:15:49', '2020-04-06 11:15:49'),
(4, 1, 'Full name', '123456789', 'name@gmail.com', 'F', 'Inzzzz', 'adulte', 'Okola', 1, '4.png', '724417', '2020-04-06 11:18:10', '2020-04-07 18:06:32'),
(5, 2, 'new Client', '123456', 'mail@gmail.com', 'F', 'assoc', 'adulte', 'adresse', 1, '5.png', '717839', '2020-04-07 18:04:31', '2020-04-07 18:06:11'),
(6, 3, 'nouveau client', '123456', 'm@gmail.com', 'M', 'association', '05 - 19', 'Nomayos', 0, '', '746114', '2020-04-08 10:33:05', '2020-04-08 10:33:05'),
(7, 4, 'test', '123456', 'test@gmail.com', 'F', 'testAssoc', 'jeune', 'adresse', 1, '', '782085', '2020-04-08 18:20:33', '2020-04-08 19:35:32'),
(8, 4, 'fullname', '12301230', 'email@gmail.com', 'F', 'association', 'jeune', 'adresse', 0, '', '116674', '2020-04-08 22:53:25', '2020-04-08 22:53:25'),
(9, 2, 'nouveau client', '12345678', 'test@gmail.com', 'M', 'testAssoc', 'adolescent', 'Okola', 0, '', '589650', '2020-04-08 23:11:59', '2020-04-08 23:11:59'),
(10, 2, 'test', '12345678', 'test@gmail.com', 'M', 'testAssoc', 'adulte', 'adresse', 0, '', '609587', '2020-04-08 23:14:46', '2020-04-08 23:14:46');

-- --------------------------------------------------------

--
-- Structure de la table `evenement`
--

CREATE TABLE `evenement` (
  `idevent` int(11) NOT NULL,
  `roomIdroom` int(11) NOT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `debut` datetime DEFAULT NULL,
  `titre` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `statut` int(1) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `evenement`
--

INSERT INTO `evenement` (`idevent`, `roomIdroom`, `logo`, `debut`, `titre`, `description`, `statut`, `createdAt`, `updatedAt`) VALUES
(1, 1, 'file-1586368981757.jpg', '2020-04-25 08:35:05', 'Terminaison', '', 1, '2020-04-05 16:53:55', '2020-04-08 21:33:16'),
(2, 2, 'file-1586368716493.jpg', '2020-04-13 06:35:05', 'Evenement 002', '', 1, '2020-04-07 10:43:01', '2020-04-08 23:39:02'),
(3, 2, 'file-1586259770414.jpg', '2020-04-13 06:35:05', 'Evenement 003', '', 1, '2020-04-07 11:42:50', '2020-04-07 11:42:50'),
(4, 2, 'file-1586369421917.jpg', '2020-04-08 06:35:05', 'Flex Before talking 1', '', 1, '2020-04-08 10:37:02', '2020-04-08 23:07:48');

-- --------------------------------------------------------

--
-- Structure de la table `reserver`
--

CREATE TABLE `reserver` (
  `idroom` int(11) NOT NULL,
  `idclient` int(11) NOT NULL,
  `debut` datetime DEFAULT NULL,
  `fin` datetime DEFAULT NULL,
  `statut` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `room`
--

CREATE TABLE `room` (
  `idroom` int(11) NOT NULL,
  `userIduser` int(11) NOT NULL,
  `label` varchar(32) DEFAULT NULL,
  `contenance` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `statut` int(1) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `room`
--

INSERT INTO `room` (`idroom`, `userIduser`, `label`, `contenance`, `description`, `statut`, `createdAt`, `updatedAt`) VALUES
(1, 1, 'salle 001', 450, '', 1, '2020-04-05 15:50:42', '2020-04-07 08:53:45'),
(2, 1, 'salle 002', 350, '', 1, '2020-04-06 14:37:16', '2020-04-07 07:44:38');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `iduser` int(11) NOT NULL,
  `cni` varchar(32) DEFAULT NULL,
  `fullname` varchar(32) DEFAULT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `statut` int(1) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`iduser`, `cni`, `fullname`, `phone`, `email`, `password`, `statut`, `createdAt`, `updatedAt`) VALUES
(1, '12345678', 'Yves Loic', '693624491', 'yves@gmail.com', '$2b$10$jXYt54bHYYOBCmdcEOWgLOi33Vkq5xbYsTPb3jFM5YrELBztT1pIu', 1, '2020-04-05 12:35:05', '2020-04-05 12:35:05');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`idclient`),
  ADD KEY `idevent` (`evenementIdevent`);

--
-- Index pour la table `evenement`
--
ALTER TABLE `evenement`
  ADD PRIMARY KEY (`idevent`),
  ADD KEY `idroom` (`roomIdroom`);

--
-- Index pour la table `reserver`
--
ALTER TABLE `reserver`
  ADD PRIMARY KEY (`idroom`,`idclient`),
  ADD KEY `idclient` (`idclient`);

--
-- Index pour la table `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`idroom`),
  ADD KEY `iduser` (`userIduser`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`iduser`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `client`
--
ALTER TABLE `client`
  MODIFY `idclient` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `evenement`
--
ALTER TABLE `evenement`
  MODIFY `idevent` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `reserver`
--
ALTER TABLE `reserver`
  MODIFY `idroom` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `room`
--
ALTER TABLE `room`
  MODIFY `idroom` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `client`
--
ALTER TABLE `client`
  ADD CONSTRAINT `client_ibfk_1` FOREIGN KEY (`evenementIdevent`) REFERENCES `evenement` (`idevent`);

--
-- Contraintes pour la table `evenement`
--
ALTER TABLE `evenement`
  ADD CONSTRAINT `evenement_ibfk_1` FOREIGN KEY (`roomIdroom`) REFERENCES `room` (`idroom`);

--
-- Contraintes pour la table `reserver`
--
ALTER TABLE `reserver`
  ADD CONSTRAINT `reserver_ibfk_1` FOREIGN KEY (`idclient`) REFERENCES `client` (`idclient`);

--
-- Contraintes pour la table `room`
--
ALTER TABLE `room`
  ADD CONSTRAINT `room_ibfk_1` FOREIGN KEY (`userIduser`) REFERENCES `user` (`iduser`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
