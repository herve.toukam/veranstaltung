const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const jwt = require('./web/jwt');
const errorHandler = require('./web/error-handler');

const app = express();

app.use(cors());

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

//define DB models
const db = require('./app/models');
db.sequelize.sync();
/**
 *  db.sequelize.sync({ force: true }).then(() => {
        console.log("Drop and re-sync db.");
    });
*/

// use JWT auth to secure the api
app.use(jwt());

// global error handler
app.use(errorHandler);

// simple route
app.get("/", (req, res) => {
    res.json({ message: "Welcome to the Event Manager application with NodeJs and Sequelize." });
});

//define routes for managing entities
require('./app/routes/client.routes')(app);
require('./app/routes/event.routes')(app);
require('./app/routes/reserver.routes')(app);
require('./app/routes/room.routes')(app);
require('./app/routes/user.routes')(app);

// set port, listen for requests
const PORT = process.env.PORT || 1234;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});