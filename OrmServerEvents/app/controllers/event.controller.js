const db = require('../models');
const Event = db.event;
const path = require('path');
const Op = db.Sequelize.Op;

// Create and Save a new Event
exports.create = (req, res, file) => {
    // Create a Event
    const event = {
        roomIdroom: req.body.roomIdroom,
        logo: file,
        debut: req.body.debut,
        titre: req.body.titre,
        description: req.body.description ? req.body.description : '',
        statut: req.body.statut ? req.body.statut : true
    };

    // Save Event in the database
    Event.create(event)
        .then(data => {
            res.status(200).send({
                message: 'success',
                data: data
            });
        })
        .catch(err => {
            res.status(500).send({
                inserted: false,
                message: err.message || 'failed',
                reference: err
            });
        });
};

// Retrieve all Events from the database.
exports.findAll = (req, res) => {
    const statut = req.query.params;
    var condition = statut ? { statut } : null;
    var events = Array();
    var nbre = 0;

    Event.findAll({ where: condition, include: [{ model: db.room, required: true }/*, { model: db.client, required: true }*/] })
        .then(data => {
            // console.log(data);
            data.forEach(el => {
                nbre += 1;
                events.push({
                    id: nbre,
                    idevent: el.dataValues.idevent,
                    idroom: el.dataValues.room,
                    clients: el.dataValues.clients,
                    logo: req.get('Origin') + "/OrmServerEvents/uploads/" + el.dataValues.logo,
                    debut: el.dataValues.debut,
                    titre: el.dataValues.titre,
                    description: el.dataValues.description,
                    consult: '<a href="maquette.html?idevent=' + el.dataValues.idevent + '"><button type="button" class="btn btn-primary">Voir<i class="fas fa-plus-circle"></i></button></a>'
                });
            });
            res.status(200).send({
                length: data.length,
                data: events
            });
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving Events.",
                reference: err
            });
        });
};

// Find a single Event with an id
exports.findOne = (req, res) => {
    const id = req.params.id;

    Event.findByPk(id)
        .then(data => {
            // console.log(data); 
            var event = {
                idevent: data.dataValues.idevent,
                idroom: data.dataValues.roomIdroom,
                logo: req.get('Origin') + "/OrmServerEvents/uploads/" + data.dataValues.logo,
                debut: data.dataValues.debut,
                titre: data.dataValues.titre,
                description: data.dataValues.description
            };
            res.status(200).send({
                data: event
            });
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Event with id=" + id,
                reference: err
            });
        });
};

// Update a Event by the id in the request
exports.editEvent = (req, res, file) => {
    const id = req.params.id;
    const event = {
        roomIdroom: req.body.roomIdroom,
        logo: file,
        debut: req.body.debut,
        titre: req.body.titre,
        description: req.body.description ? req.body.description : '',
        statut: req.body.statut ? req.body.statut : true
    };
    
    Event.update(req.body, { where: { idevent: id } })
        .then(num => {
            if (num == 1) {
                res.status(200).send({
                    message: "success"
                });
            } else {
                res.status(200).send({
                    message: `Cannot update Event with id=${id}. Maybe Event was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Event with id=" + id,
                reference: err
            });
        });
};

// Delete a Event with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Event.destroy({ where: { id: id } })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Event was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Event with id=${id}. Maybe Event was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Event with id=" + id,
                reference: err
            });
        });
};