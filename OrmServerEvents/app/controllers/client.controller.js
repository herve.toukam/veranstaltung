const db = require('../models');
const Client = db.client;
const Op = db.Sequelize.Op;

// Create and Save a new Client
exports.create = (req, res, file) => {
    // Create a Client
    const client = {
        evenementIdevent: req.body.evenementIdevent,
        fullname: req.body.fullname,
        phone: req.body.phone,
        email: req.body.email,
        sexe: req.body.sexe,
        association: req.body.association,
        categorieage: req.body.categorieage,
        adresse: req.body.adresse,
        code: req.body.code,
        signature: file || '',
        etat: req.body.etat ? req.body.etat : false
    };

    // Save Client in the database
    Client.create(client)
        .then(data => {
            res.status(200).send({
                message: 'success',
                data: data
            });
        })
        .catch(err => {
            res.status(500).send({
                inserted: false,
                message: err.message || 'failed',
                reference: err
            });
        });
};

// Retrieve all Clients from the database.
exports.findAll = (req, res) => {
    const id = req.query.idevent;
    var clients = Array();
    var jeune = 0, ado = 0, adulte = 0, segnor = 0, total = 0;

    Client.findAll({ where: { evenementIdevent: id } })
        .then(data => {
            data.forEach(el => {
                total +=1;
                ado += el.dataValues.categorieage == 'adolescent' ? 1 : 0;
                jeune += el.dataValues.categorieage == 'jeune' ? 1 : 0;
                adulte += el.dataValues.categorieage == 'adulte' ? 1 : 0;
                segnor += el.dataValues.categorieage == 'segnor' ? 1 : 0;
                if (el.dataValues.etat == 0) {
                    clients.push({
                        id: total,
                        idevent: el.dataValues.evenementIdevent,
                        idclient: el.dataValues.idclient,
                        fullname: el.dataValues.fullname,
                        phone: el.dataValues.phone,
                        email: el.dataValues.email,
                        sexe: el.dataValues.sexe,
                        association: el.dataValues.association,
                        categorieage: el.dataValues.categorieage,
                        adresse: el.dataValues.adresse,
                        code: el.dataValues.code,
                        pp: el.dataValues.signature != '' ? req.get('Origin') + "/OrmServerEvents/clients/" + el.dataValues.signature: '',
                        signature: '<a href="signature/index.html?idclient=' + el.dataValues.idclient + '&lieux=serv"> Sign </a>'
                    });
                } else {
                    clients.push({
                        id: total,
                        idevent: el.dataValues.idevent,
                        idclient: el.dataValues.idclient,
                        fullname: el.dataValues.fullname,
                        phone: el.dataValues.phone,
                        email: el.dataValues.email,
                        sexe: el.dataValues.sexe,
                        association: el.dataValues.association,
                        categorieage: el.dataValues.categorieage,
                        adresse: el.dataValues.adresse,
                        code: el.dataValues.code,
                        pp: el.dataValues.signature != '' ? req.get('Origin') + "/OrmServerEvents/clients/" + el.dataValues.signature: '',
                        signature: '<i class="fa fa-check"></i>'
                    });
                }
            });
            res.status(200).send({
                length: data.length,
                data: clients,
                stat: [
                    {
                        name: 'Adolescents',
                        y: ado
                    },
                    {
                        name: 'Jeunes',
                        y: jeune
                    },
                    {
                        name: 'Adultes',
                        y: adulte
                    },
                    {
                        name: 'Segnors',
                        y: segnor
                    },
                ]
            });
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving Clients.",
                reference: err
            });
        });
};

// Find a single Client with an id
exports.findById = (req, res) => {
    const idclient = req.params.id;

    Client.findByPk(idclient)
        .then(data => {
            var client = {
                idclient: data.dataValues.idclient,
                evenementIdevent: data.dataValues.evenementIdevent,
                fullname: data.dataValues.fullname,
                phone: data.dataValues.phone,
                email: data.dataValues.email,
                sexe: data.dataValues.sexe,
                association: data.dataValues.association,
                categorieage: data.dataValues.categorieage,
                adresse: data.dataValues.adresse,
                code: data.dataValues.code,
                signature: data.dataValues.signature != '' ? req.get('Origin') + "/OrmServerEvents/clients/" + data.dataValues.signature: ''
            };
            res.status(200).send({
                data: client
            });
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Client with id=" + idclient,
                reference: err
            });
        });
};

// Update a Client by the id in the request
exports.update = (req, res, file) => {
    const id = req.params.id;
    var client = {
        signature: file,
        etat: req.body.etat
    }

    Client.update(client, { where: { idclient: id } })
        .then(num => {
            console.log(num);
            if (num == 1) {
                res.status(200).send({
                    message: "success",
                    data: num
                });
            } else {
                res.status(200).send({
                    message: `Cannot update Client with id=${id}. Maybe Client was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Client with id=" + id,
                reference: err
            });
        });
};

// Delete a Client with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Client.destroy({ where: { id: id } })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Client was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Client with id=${id}. Maybe Client was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Client with id=" + id,
                reference: err
            });
        });
};

// Find a single Client with a code
exports.findByCode = (req, res) => {
    const code = req.body.code;

    Client.findOne({ where: { code: code } })
        .then(data => {
            var client = {
                idclient: data.dataValues.idclient,
                evenementIdevent: data.dataValues.evenementIdevent,
                fullname: data.dataValues.fullname,
                phone: data.dataValues.phone,
                email: data.dataValues.email,
                sexe: data.dataValues.sexe,
                association: data.dataValues.association,
                categorieage: data.dataValues.categorieage,
                adresse: data.dataValues.adresse,
                code: data.dataValues.code,
                signature: data.dataValues.signature != '' ? req.get('Origin') + "/OrmServerEvents/clients/" + data.dataValues.signature: '',
            };
            res.status(200).send({
                data: client
            });
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Client with code=" + code,
                reference: err
            });
        });
};