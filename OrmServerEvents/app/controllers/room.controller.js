const db = require('../models');
const Room = db.room;
const Op = db.Sequelize.Op;

// Create and Save a new Room
exports.create = (req, res) => {
    // Create a Room
    const room = {
        iduser: req.body.iduser,
        label: req.body.label,
        contenance: req.body.contenance,
        description: req.body.description ? req.body.description : '',
        statut: req.body.statut ? req.body.statut : true
    };

    // Save Room in the database
    Room.create(room)
        .then(data => {
            res.status(200).send({
                message: 'success',
                data: data
            });
        })
        .catch(err => {
            res.status(500).send({
                inserted: false,
                message: err.message || 'failed',
                reference: err
            });
        });
};

// Retrieve all Rooms from the database.
exports.findAll = (req, res) => {
    const statut = req.query.params;
    var condition = statut ? { statut } : null;
    var Rooms = Array();
    Room.findAll({ where: condition })
        .then(data => {
            data.forEach(el => {
                Rooms.push({
                    idroom: el.dataValues.idroom,
                    label: el.dataValues.label,
                    contenance: el.dataValues.contenance,
                    description: el.dataValues.description,
                    options: '<button class="btn btn-success" data-toggle="modal" data-target="#edit-room" data-id="'+el.dataValues.idroom+','+el.dataValues.label+','+el.dataValues.contenance+'"><i class="fas fa-edit"></i></button><button class="btn btn-danger" data-toggle="modal" data-target="#del-room" onclick="$(\'#id_del\').val(' + el.dataValues.idroom + ')"><i class="fas fa-trash"></i></button>'
                })
            });
            res.status(200).send({
                length: data.length,
                data: Rooms
            });
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving Rooms.",
                reference: err
            });
        });
};

// Find a single Room with an id
exports.findOne = (req, res) => {
    const id = req.params.id;

    Room.findByPk(id)
        .then(data => {
            res.status(200).send({
                data: data
            });
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Room with id=" + id,
                reference: err
            });
        });
};

// Update a Room by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;

    Room.update(req.body, { where: { idroom: id } })
        .then(num => {
            console.log(num);
            if (num == 1) {
                res.send({
                    message: "success"
                });
            } else {
                res.send({
                    message: `Cannot update Room with id=${id}. Maybe Room was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Room with id=" + id,
                reference: err
            });
        });
};

// Delete a Room with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Room.destroy({ where: { id: id } })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Room was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Room with id=${id}. Maybe Room was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Room with id=" + id,
                reference: err
            });
        });
};