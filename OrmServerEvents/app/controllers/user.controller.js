const db = require('../models');
const User = db.user;
const Op = db.Sequelize.Op;
const bcrypt = require('bcrypt');
const config = require('../../config.json');
const jwt = require('jsonwebtoken');

// Create and Save a new User
exports.create = (req, res) => {
    // Create a User
    const user = {
        fullname: req.body.fullname,
        phone: req.body.phone,
        cni: req.body.cni,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10)),
        statut: req.body.statut ? req.body.statut : true
    };

    // Save User in the database
    User.create(user)
        .then(data => {
            res.status(200).send({
                message: 'success',
                data: data
            });
        })
        .catch(err => {
            res.status(500).send({
                inserted: false,
                message: err.message || 'failed',
                reference: err
            });
        });
};

// Retrieve all Users from the database.
exports.findAll = (req, res) => {
    const title = req.query.params;
    var condition = title ? { title: { [Op.like]: `%${title}%` } } : null;

    User.findAll({ where: condition })
        .then(data => {
            res.status(200).send({
                length: data.length,
                data: data
            });
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving Users.",
                reference: err
            });
        });
};

// Find a single User with an id
exports.findOne = (req, res) => {
    const id = req.params.id;

    User.findByPk(id)
        .then(data => {
            res.status(200).send({
                data: data
            });
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving User with id=" + id,
                reference: err
            });
        });
};

// Update a User by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;

    const user = {
        fullname: req.body.fullname,
        phone: req.body.phone,
        cni: req.body.cni,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10)),
        statut: req.body.statut ? req.body.statut : true
    };

    User.update(user, { where: { iduser: id } })
        .then(num => {
            if (num == 1) {
                res.status(200).send({
                    message: "success"
                });
            } else {
                res.status(200).send({
                    message: `Cannot update User with id=${id}. Maybe User was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating User with id=" + id,
                reference: err
            });
        });
};

// Delete a User with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    User.destroy({ where: { id: id } })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "User was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete User with id=${id}. Maybe User was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete User with id=" + id,
                reference: err
            });
        });
};

exports.login = (req, res) => {
    const params = {
        email: req.body.email,
        password: req.body.password
    };

    if (!params.email || !params.password) {
        res.statut(400).send({
            message: 'no data found!'
        });
    } else {
        User.findOne({ where: { email: params.email } })
            .then(user => {
                // console.log(user.dataValues);
                var date = new Date();
                var expire = new Date(date);
                expire.setDate(expire.getDate() + 1);
                if (bcrypt.compareSync(params.password, user.dataValues.password)) {
                    // console.log('test password');
                    const token = jwt.sign({
                        sub: user.dataValues.iduser,
                        iat: date.getTime(),
                        exp: expire.getTime()
                    }, config.secret);
                    res.status(201).send({
                        data: user,
                        token: token,
                        expireAt: new Date(expire)
                    });
                } else {
                    res.status(500).send({
                        message: 'Bad credentials'
                    });
                }
            }).catch(err => {
                res.status(500).send({
                    message: 'Bad credentials',
                    reference: err
                });
            });
    }
};