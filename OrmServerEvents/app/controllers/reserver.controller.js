const db = require('../models');
const Reserver = db.reserver;
const Op = db.Sequelize.Op;

// Create and Save a new Reserver
exports.create = (req, res) => {
    // Create a Reserver
    const reserver = {
        idroom: req.body.idroom,
        idclient: req.body.idclient,
        debut: req.body.debut,
        fin: req.body.fin,
        statut: req.body.statut ? req.body.statut : true
    };

    // Save Reserver in the database
    Reserver.create(reserver)
        .then(data => {
            res.status(200).send({
                message: 'success',
                data: data
            });
        })
        .catch(err => {
            res.status(500).send({
                inserted: false,
                message: err.message || 'failed',
                reference: err
            });
        });
};

// Retrieve all Reservers from the database.
exports.findAll = (req, res) => {
    const title = req.query.params;
    var condition = title ? { title: { [Op.like]: `%${title}%` } } : null;

    Reserver.findAll({ where: condition })
        .then(data => {
            res.status(200).send({
                length: data.length,
                data: data
            });
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving Reservations.",
                reference: err
            });
        });
};

// Find a single Reserver with an id
exports.findOne = (req, res) => {
    const idroom = req.params.idroom;
    const idclient = req.params.idclient;

    Reserver.findOne({where: {idroom, idclient}})
        .then(data => {
            res.status(200).send({
                data: data
            });
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Reservation with id=" + id,
                reference: err
            });
        });
};

// Update a Reserver by the id in the request
exports.update = (req, res) => {
    const id1 = req.params.idroom;
    const id2 = req.params.idclient;

    Reserver.update(req.body, { where: { idroom: id1, idclient: id2 } })
        .then(num => {
            console.log(num);
            if (num == 1) {
                res.status(200).send({
                    message: "success",
                    data: num
                });
            } else {
                res.status(200).send({
                    message: `Cannot update Reservation with id=${id}. Maybe Reservation was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Reservation with id=" + id,
                reference: err
            });
        });
};

// Delete a Reserver with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Reserver.destroy({ where: { id: id } })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Reservation was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Reservation with id=${id}. Maybe Reservation was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Reservation with id=" + id,
                reference: err
            });
        });
};