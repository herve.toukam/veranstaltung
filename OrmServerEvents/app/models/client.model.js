/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    const Client = sequelize.define('client', {
        idclient: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        evenementIdevent: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'evenement',
                key: 'idevent'
            }
        },
        fullname: {
            type: DataTypes.STRING(32),
            allowNull: true
        },
        phone: {
            type: DataTypes.STRING(32),
            allowNull: false
        },
        email: {
            type: DataTypes.STRING(50),
            allowNull: false
        },
        sexe: {
            type: DataTypes.CHAR(1),
            allowNull: false
        },
        association: {
            type: DataTypes.STRING(50),
            allowNull: false
        },
        categorieage: {
            type: DataTypes.STRING(50),
            allowNull: false
        },
        adresse: {
            type: DataTypes.STRING(75),
            allowNull: false
        },
        etat: {
            type: DataTypes.INTEGER(11),
            allowNull: false
        },
        signature: {
            type: DataTypes.STRING(255),
            allowNull: true
        },
        code: {
            type: DataTypes.STRING(25),
            allowNull: false
        }
    }, {
        tableName: 'client'
    });

    return Client;
};
