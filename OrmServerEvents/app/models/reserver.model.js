/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    const Reservation = sequelize.define('reserver', {
        idroom: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        idclient: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'client',
                key: 'idclient'
            }
        },
        debut: {
            type: DataTypes.DATE,
            allowNull: true
        },
        fin: {
            type: DataTypes.DATE,
            allowNull: true
        },
        statut: {
            type: DataTypes.INTEGER(11),
            allowNull: true
        }
    }, {
        tableName: 'reserver'
    });

    return Reservation;
};
