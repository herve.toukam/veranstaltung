/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    const Event = sequelize.define('evenement', {
        idevent: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        roomIdroom: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'room',
                key: 'idroom'
            }
        },
        logo: {
            type: DataTypes.STRING(255),
            allowNull: true
        },
        debut: {
            type: DataTypes.DATE,
            allowNull: true
        },
        titre: {
            type: DataTypes.STRING(255),
            allowNull: false
        },
        description: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        statut: {
            type: DataTypes.INTEGER(1),
            allowNull: true
        }
    }, {
        tableName: 'evenement'
    });

    return Event;
};
