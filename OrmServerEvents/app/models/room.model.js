/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    const Room = sequelize.define('room', {
        idroom: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        userIduser: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'user',
                key: 'iduser'
            }
        },
        label: {
            type: DataTypes.STRING(32),
            allowNull: true
        },
        contenance: {
            type: DataTypes.INTEGER(11),
            allowNull: true
        },
        description: {
            type: DataTypes.STRING(255),
            allowNull: true
        },
        statut: {
            type: DataTypes.INTEGER(1),
            allowNull: true
        }
    }, {
        tableName: 'room'
    });

    return Room;
};
