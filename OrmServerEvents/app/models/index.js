const dbConfig = require('../config/db.config');
const Sequelize = require('sequelize');

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
    host: dbConfig.HOST,
    dialect: dbConfig.dialect,
    operatorsAliases: false,

    pool: {
        max: dbConfig.pool.max,
        min: dbConfig.pool.min,
        acquire: dbConfig.pool.acquire,
        idle: dbConfig.pool.idle
    }
});

const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;
//define tables 
db.client = require('./client.model')(sequelize, Sequelize);
db.event = require('./event.model')(sequelize, Sequelize);
db.reserver = require('./reserver.model')(sequelize, Sequelize);
db.room = require('./room.model')(sequelize, Sequelize);
db.user = require('./user.model')(sequelize, Sequelize);

db.event.belongsTo(db.room);
db.event.hasMany(db.client);
db.room.belongsTo(db.user);

module.exports = db;