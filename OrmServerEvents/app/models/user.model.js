/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    const User = sequelize.define('user', {
        iduser: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        cni: {
            type: DataTypes.STRING(32),
            allowNull: true
        },
        fullname: {
            type: DataTypes.STRING(32),
            allowNull: true
        },
        phone: {
            type: DataTypes.STRING(32),
            allowNull: true
        },
        email: {
            type: DataTypes.STRING(50),
            allowNull: false
        },
        password: {
            type: DataTypes.STRING(128),
            allowNull: false
        },
        statut: {
            type: DataTypes.INTEGER(1),
            allowNull: false
        }
    }, {
        tableName: 'user'
    });

    return User;
};
