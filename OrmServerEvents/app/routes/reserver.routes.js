module.exports = app => {
    const reserver = require('../controllers/reserver.controller');

    var router = require('express').Router();

    // Create a new reservation
    router.post("/add", reserver.create);

    // Retrieve all reserver
    router.get("/", reserver.findAll);

    // Retrieve a single reservation with id
    router.get("/:idroom/:idclient", reserver.findOne);

    // Update a reservation with id
    router.put("/:idroom/:idclient", reserver.update);

    // Delete a reservation with id
    router.delete("/del/:id", reserver.delete);

    app.use('/api/reserver', router);
};