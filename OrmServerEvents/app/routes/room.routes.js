module.exports = app => {
    const room = require('../controllers/room.controller');

    var router = require('express').Router();

    // Create a new room
    router.post("/add", room.create);

    // Retrieve all room
    router.get("/", room.findAll);

    // Retrieve a single room with id
    router.get("/:id", room.findOne);

    // Update a room with id
    router.put("/:id", room.update);

    // Delete a room with id
    router.delete("/del/:id", room.delete);

    app.use('/api/room', router);
};