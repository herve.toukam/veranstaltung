const expressJwt = require('express-jwt');
const config = require('../config.json');

module.exports = jwt;

function jwt() {
    const { secret } = config;
    return expressJwt({ secret }).unless({
        path: ['/api/users/login',
            '/',
            '/api/users/add',
            '/api/event',
            '/api/client/add',
            '/api/client/code',
            new RegExp('/api/client.*/', 'i')] // public routes that don't require authentication
    });
}